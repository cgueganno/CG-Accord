#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "audiorecorder.h"

void CgAccordPlugin::registerTypes(const char *uri) {
    //@uri AudioRecorder
    qmlRegisterSingletonType<AudioRecorder>(uri, 1, 0, "AudioRecorder", [](QQmlEngine*, QJSEngine*) -> QObject* { return new AudioRecorder; });
}
