#ifndef CGACCORD_H
#define CGACCORD_H

#include <QObject>
#include <math.h>


class CgAccord: public QObject {
    Q_OBJECT

private:
     double rTEQJ;
     double rOP;
     float cents;
     float lastHz;
     float lastCents; 
     double r;
     double logr;
     double c;
     double lnc;
     double f0;
     bool isNull;
     int nNote;   
     
     double DIAPASON;
     int T;

    double frand(double a, double b);

public:
    CgAccord();
    ~CgAccord() = default;

    Q_INVOKABLE void speak();
    Q_INVOKABLE double frequence();
    Q_INVOKABLE int getCents();
    Q_INVOKABLE void setDiapason(double d);
    Q_INVOKABLE void setT(int t);
    Q_INVOKABLE int fromHz(float hz);
};

#endif
