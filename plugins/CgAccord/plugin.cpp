#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "cgaccord.h"

void CgAccordPlugin::registerTypes(const char *uri) {
    //@uri CgAccord
    qmlRegisterSingletonType<CgAccord>(uri, 1, 0, "CgAccord", [](QQmlEngine*, QJSEngine*) -> QObject* { return new CgAccord; });
}
