#include <QDebug>
#include "cgaccord.h"

CgAccord::CgAccord() {
      rTEQJ = pow(3./2., 1./7.);
      rOP = pow(2., 1./12.);
	  r = pow(2., 1./12.);
      logr = log(r);
      c = pow(r, 1./100.);
      lnc = log(c);
      isNull = true;
      nNote = 0;
      DIAPASON = 440;
      setT(50); // T = 50 + calcul de R
}

void CgAccord::speak() {
    qDebug() << "hello world!";
}

double CgAccord::frequence() {
	 return frand(430, 450);
     // return 880;
}

int CgAccord::getCents() {
        return round(cents);
}

void CgAccord::setDiapason(double d) { DIAPASON = d; }

void CgAccord::setT(int t) {
   T = t;
   if (T == 0)  r = rOP;
   else if (T == 100) r = rTEQJ;
   else r = rOP + (rTEQJ - rOP) * T / 100.;
}

int CgAccord::fromHz(float hz) {
   if (hz < 0) {
        isNull = true;
            nNote = 0;
            cents = 0;
            return -1;
   }

   isNull = false;
   double semi = log(hz/DIAPASON) / logr;
   int roundedSemi = round(semi);
   int note = (roundedSemi % 12 + 12) % 12;
   f0 = DIAPASON * pow(r, roundedSemi);
   float newCents = (float)(log(hz/f0) / lnc);

   if (note==nNote && abs(hz - lastHz) / hz < 0.5) {
       cents = (lastCents + newCents) / 2;
   } else cents = newCents;
        
   nNote = note;
   lastHz = hz;
   lastCents = newCents;
   return note;
}

double CgAccord::frand(double a, double b){
    return ( rand()/(double)RAND_MAX ) * (b-a) + a;
}
