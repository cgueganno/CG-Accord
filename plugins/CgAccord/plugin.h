#ifndef CGACCORDPLUGIN_H
#define CGACCORDPLUGIN_H

#include <QQmlExtensionPlugin>

class CgAccordPlugin : public QQmlExtensionPlugin {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri);
};

#endif
