import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import CgAccord 1.0
import AudioRecorder 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'cgaccord.cg'
    automaticOrientation: true
    width: units.gu(45)
    height: units.gu(75)
    property bool teqj: true
    
    property double diapason: 440
    property double diapasonRef: 440
    property double temperament: 50
    property double angle: 0
    property bool libre: true;


	property var listeNotes: ["La", "Si♭", "Si", "Do", "Do♯", "Ré", "Mi♭", "Mi", "Fa", "Fa♯", "Sol", "Sol♯"]
	property var rangNote: 0
	
	property double f0: 440
	property int cents:0


    Page {
        anchors.fill: parent

        header: PageHeader {
            id: pageHeader
            title: i18n.tr('CGaccord')
            trailingActionBar.actions: [
              Action {
                iconName: "info"
                onTriggered: root.push(Qt.resolvedUrl("Param.qml"))
              },
              Action {
                iconSource: "img/tuning-fork.svg"
                onTriggered: root.push(Qt.resolvedUrl("Param.qml"))
              }
           ]
        }
     
  
        Connections {
            target: AudioRecorder
                    onFrequencyChanged: {
						   f0 =  Math.round(AudioRecorder.getFrequence()*100) / 100;
						   if (f0 != 0) rangNote = CgAccord.fromHz(f0);
						   else rangNote = -1;
						   if (rangNote != -1) {
							    cents = CgAccord.getCents();
								idNote.text = i18n.tr("" + listeNotes[rangNote]); 
								idHz.text = i18n.tr("" + f0 + "Hz");
								if (cents>0) idCents.text =  i18n.tr("+" + cents);
								else idCents.text = i18n.tr("" + cents);
								if (Math.abs(cents) >6) idCents.color = "#df3a3a";
								else idCents.color = "#199a37";
								angle =  CgAccord.getCents();
							    if (Math.abs(angle) == 0) aiguille.source = "img/aBleu.svg"
							    else if (Math.abs(angle) < 6) aiguille.source = "img/aVert.svg"
							    else aiguille.source = "img/aRouge.svg"
							    aiguille.rotation = angle/2.4;					
						   } 
						   else {
							   idNote.text = i18n.tr("-");
							   idHz.text = i18n.tr("Hz");
							   idCents.text = "0";
							   dCents.color = "#199a37";
							   aiguille.source = "img/aBleu.svg";
							   aiguille.rotation = 0;	
						   }
					}
		}


      // Timer {
       //     id: timer1
       //     interval: 500; running: true; repeat: true
       //      property bool sensPlus: true
       //      onTriggered: {
 			   // f0 =  Math.round(CgAccord.frequence()*100) / 100;
 			   // f0 =  Math.round(AudioRecorder.getFrequence()*100) / 100;
		// 	   if (f0 != 0) rangNote = CgAccord.fromHz(f0);
		// 	   else rangNote = -1;
		// // 	   if (rangNote != -1) {
         //            idNote.text = i18n.tr("" + listeNotes[rangNote]); 
        //             idHz.text = i18n.tr("" + f0 + "Hz");
		// 	   } 
        //        else {
		// 		   idNote.text = i18n.tr("-");
		// 		   idHz.text = i18n.tr("Hz");
		// 	   }
      //          angle =  CgAccord.getCents();
      //          if (Math.abs(angle) == 0) aiguille.source = "img/aBleu.svg"
     //           else if (Math.abs(angle) < 6) aiguille.source = "img/aVert.svg"
     //           else aiguille.source = "img/aRouge.svg"
     //           aiguille.rotation = angle/2.4;
    //         }
    //     }

        Column {
            id: pagePrincipale
            anchors {
                topMargin: units.gu(8)
                fill: parent
                horizontalCenter: pageHeader.horizontalCenter
            }
            spacing: units.gu(1)


            Grid {
                id: tableauDeBord1a
                columns: 6
                Label{text:  i18n.tr(' 415: '); }
                Switch{
                   id: s415
                   checked: false
                   onClicked: {
                     if (checked) {
                        diapason = diapasonRef = 415
                        s440.checked = false
                        s442.checked = false
                        slDiapason.value = 50
                        valDiapason.text=i18n.tr('' + diapason + 'Hz');
                        CgAccord.setDiapason(diapason);
                     }
                  }
                }
                Label{
                    anchors.leftMargin: units.gu(3)
                    text:  i18n.tr('   440: ')
                }

                Switch{
                   id: s440
                   checked: true
                   onClicked: {
                     if (checked) {
                        diapason = diapasonRef = 440
                        s415.checked = false
                        s442.checked = false
                        slDiapason.value = 50
                        valDiapason.text=i18n.tr('' + diapason + 'Hz')
                        CgAccord.setDiapason(diapason);
                     }
                  }
                }

                Label{
                    anchors.leftMargin: units.gu(3)
                    text:  i18n.tr('   442: ')
                }

                Switch{
                   id: s442
                   checked: false
                   onClicked: {
                     if (checked) {
                        diapason = diapasonRef = 442
                        s440.checked = false
                        s415.checked = false
                        slDiapason.value = 50
                        valDiapason.text=i18n.tr('' + diapason + 'Hz')
                        CgAccord.setDiapason(diapason)
                     }
                  }
                }
              }

             Grid {
                id: tableauDeBord1b
                columns: 3
                anchors.top: tableauDeBord1a.bottom
                spacing: units.gu(1)
                Label{
                        text:  i18n.tr('  D:')
                }
                Slider {
                    id: slDiapason
                    minimumValue: 0
                    maximumValue: 100
                    value: 50
                    width: units.gu(28)
                    onValueChanged:{
                       diapason = Math.round(10*(diapasonRef + 2*(slDiapason.value - 50)/100))/10
                       valDiapason.text =  i18n.tr('' + diapason + 'Hz')
                       CgAccord.setDiapason(diapason)
                    }
                }
                Label{
                   id: valDiapason
                   font.family:  i18n.tr("Cursive")
                   font.bold: true
                   text:  i18n.tr('' + diapason + 'Hz')
                }
             }


             Grid {
                id: tableauDeBord2a
                anchors.top: tableauDeBord1b.bottom
                columns: 4
                Label{
                        text:  i18n.tr(' Octaves: ')
                }
                Switch{
                   id: sToctave
                   checked: false
                   onClicked: {
                     if (checked) {
                        sTquinte.checked = false
                        temperament = 0
                        valTemperament.text =  i18n.tr('' + temperament +'%')
                        slT.value = 0
                        CgAccord.setT(temperament);
                     }
                  }
                }
                Label{
                    anchors.leftMargin: units.gu(3)
                    text:  i18n.tr('   Quintes: ')
                }

                Switch{
                   id: sTquinte
                   checked: false
                   onClicked: {
                     if (checked) {
                       sToctave.checked = false
                       temperament = 100
                       valTemperament.text =  i18n.tr('' + temperament +'%')
                       slT.value = 100
                       CgAccord.setT(temperament);
                     }
                  }
                }
             }
             Grid {
                id: tableauDeBord2b
                anchors.top: tableauDeBord2a.bottom
                columns: 3

                Label{
                        text:  i18n.tr('  T:')
                }
                Slider {
                    id: slT
                    minimumValue: 0
                    maximumValue: 100
                    value: 50
                    width: units.gu(28)
                    onValueChanged:{
                       temperament = Math.round(slT.value)
                       CgAccord.setT(temperament);
                       valTemperament.text =  i18n.tr('' + temperament +'%')
                       if (temperament == 0) {
                          sToctave.checked = true
                          sTquinte.checked = false
                       } else if (temperament == 100) {
                          sToctave.checked = false
                          sTquinte.checked = true
                       } else {
                          sToctave.checked = false
                          sTquinte.checked = false
                       }
                    }
                }
                Label{
                   id: valTemperament
                   font.family:  i18n.tr("Cursive")
                   font.bold: true
                   text: i18n.tr('' + temperament +'%')
                }
            }

            ColumnLayout {
              id: cadran
              anchors.top: tableauDeBord2b.bottom
              anchors.bottom: parent.bottom
              anchors.bottomMargin: units.gu(1)
              anchors.leftMargin: units.gu(2)
              anchors.rightMargin: units.gu(2)

              width: parent.width

              Image {
                   id: fond
                   anchors {
                        
                        left: cadran.left; right: cadran.right
                        bottom: cadran.bottom; top: cadran.top
                        leftMargin: units.gu(1)
                        rightMargin: units.gu(1)
                   }
                   source: "img/winnie.jpg"

                   Image {
                      id: aiguille
                      height: fond.height * 0.9
                      width: units.gu(4)
                      anchors {
                          horizontalCenter: parent.horizontalCenter
                          bottom: parent.bottom
                      }
                      source: "img/aBleu.svg"
                      transformOrigin: Item.Bottom
                      rotation: 0
                   }

                 Image {
                      id: idCadran
                      height: fond.height * 0.15;
                      width: fond.width;
                      anchors {
                          horizontalCenter: parent.horizontalCenter
                          top: parent.top
                      }
                      source: "img/cadran.svg"
                      transformOrigin: Item.Bottom
                      rotation: 0
                   }

				   Label {
					  anchors {
						  top: parent.top
						  left: parent.horizontalCenter
						  leftMargin: units.gu(5);
                          topMargin: units.gu(28);
					  }
					  id: idNote
                      font.family:  i18n.tr("Cursive")
                      font.bold: true
                      font.pixelSize: 40
                      color: "#e20f0f"
					  text: "-"
					  
					  Label {
					   anchors {
						  left: parent.left
						  top: parent.top 
						  leftMargin: units.gu(0)
                          topMargin: units.gu(4)
					   }
					   id: idHz
                       font.family:  i18n.tr("Cursive")
                       font.bold: true
                       font.pixelSize: 20
					   text: "Hz:"
					 }
					}
					
					Label {
					  anchors {
						  top: parent.top
						  left: parent.left
						  leftMargin: units.gu(1);
                          topMargin: units.gu(8);
					  }
					  id: idCents;
                      font.family:  i18n.tr("Cursive");
                      font.bold: true;
                      font.pixelSize: 30;
                      color: "#e20f0f";
					  text: "0";					
					}
              }
            }
         } // Column

    } // Page

    Component.onCompleted: {
		CgAccord.speak();
		AudioRecorder.speak();
		CgAccord.setDiapason(diapason)
		CgAccord.setT(temperament) 
		AudioRecorder.record();
	}
}
