import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.0

import cg 1.0

Page {
    id: param
    header: PageHeader {
        id: pageHeader
        title: i18n.tr("About")
    }
           
    Column {
        id: cparam
        anchors.fill: parent
        contentHeight: aboutColumn.height

        Column {
            id: aboutColumn
            anchors.left: parent.left
            anchors.leftMargin: units.gu(2)
            anchors.right: parent.right
            anchors.rightMargin: units.gu(2)

            spacing: units.gu(3)
            
            Label {
              text:  i18n.tr('Salut Claude!!!')
            }
            
        }
     }

}
 
